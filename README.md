# integrity-game

The greatest and best game in the world.

#### Dependencies

* Java 1.8
* Gradle

#### Development Config

* include the assets folder in your working directory (under Run/Debug Configurations in IntelliJ)
* run the DesktopLauncher class under desktop/src/main/java/com/integrity/games/desktop/DesktopLauncher.java


#### Hard coded keys during dev

- F: increase fuel
- B: increase speed = move quicker
- N: increase stamina = mover further (better fuel)
- M: increase strength = take more damage