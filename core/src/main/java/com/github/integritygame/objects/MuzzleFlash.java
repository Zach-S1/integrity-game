package com.github.integritygame.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.github.integritygame.util.AssetManager;
import com.github.integritygame.util.GameManager;

/**
 * An object class for the muzzle flash used when Tanks fire.
 *
 * See also {@link Tank}
 * @author Josh Williamson
 */
public class MuzzleFlash {

    private AssetManager assetManager;
    private Texture muzzleFlashTexture;
    private TextureRegion muzzleFlashTextureRegion;

    /**
     * constructor for muzzle flash
     */
    public MuzzleFlash() {
        assetManager = AssetManager.getInstance();
        muzzleFlashTexture = assetManager.getMuzzleFlashTexture();
        muzzleFlashTextureRegion = new TextureRegion(muzzleFlashTexture);
    }

    /**
     * The main method used to draw muzzle flash on screen
     *
     * @param tank
     */
    public void animateMuzzleFlash(SpriteBatch sprite, Tank tank) {
        if (tank.isRightSide()) {
            muzzleFlashTextureRegion.flip(!muzzleFlashTextureRegion.isFlipX(), false);
            sprite.draw(muzzleFlashTextureRegion, tank.getBarrelEnd().x-50, tank.getBarrelEnd().y - 15, 50, 30);
        } else {
            muzzleFlashTextureRegion.flip(muzzleFlashTextureRegion.isFlipX(), false);
            sprite.draw(muzzleFlashTextureRegion, tank.getBarrelEnd().x, tank.getBarrelEnd().y - 15, 50, 30);
        }
    }

}
