package com.github.integritygame.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.github.integritygame.objects.BulletData;

import java.util.*;

public class AssetManager {

    private static AssetManager instance;

    public enum Background {DESERT, GRASS, HOME}

    public enum TankStyles {BLUE_TANK, GREEN_TANK, LIGHT_GREEN_TANK}

    private static Skin skin;
    private static Skin customButton;
    private static String helpText;
    private static BitmapFont titleFont;
    private static BitmapFont defaultFont;
    private static Map<Background, FileHandle> backgrounds;
    private static Map<BulletData.BulletName, Texture> bullets;
    private static Map<TankStyles, Texture[]> tankTextures;
    private static Texture muzzleFlash;
    private static Map<Background, FileHandle> foregrounds;
    private static Map<PowerUp, Texture> powerups;
    private static Map<Upgrade, Texture> upgrades;

  public enum PowerUp {
        MONEY, FUEL;

        public static PowerUp getRandomPowerUp() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }

    public enum Upgrade {
        STAMINA, SPEED, STRENGTH
    }

    private AssetManager() {
        initAssets();
    }

    private void initAssets() {
        skin = new Skin(Gdx.files.internal("visui/assets/uiskin.json"));

        helpText = Gdx.files.internal("text/helpText.txt").readString();

        customButton = new Skin();
        customButton.addRegions(new TextureAtlas("buttons/simpleButton.txt"));

        titleFont = new BitmapFont(Gdx.files.internal("fonts/defused.fnt"));
        defaultFont = new BitmapFont();

        backgrounds = new HashMap<>();
        backgrounds.put(Background.DESERT, Gdx.files.internal("backgrounds/desert-background.jpg"));
        backgrounds.put(Background.GRASS, Gdx.files.internal("backgrounds/green-bg-no-ground.jpeg"));
        backgrounds.put(Background.HOME, Gdx.files.internal("backgrounds/tank-main-menu-background.jpeg"));

        foregrounds = new HashMap<>();
        foregrounds.put(Background.DESERT, Gdx.files.internal("backgrounds/desert-ground.jpeg"));
        foregrounds.put(Background.GRASS, Gdx.files.internal("backgrounds/green-ground.jpeg"));

        bullets = new HashMap<>();
        bullets.put(BulletData.BulletName.SMALL, new Texture(Gdx.files.internal("projectiles/ProjectileBlack.png")));
        bullets.put(BulletData.BulletName.MEDIUM, new Texture(Gdx.files.internal("projectiles/ProjectileBlackGreen.png")));
        bullets.put(BulletData.BulletName.LARGE, new Texture(Gdx.files.internal("projectiles/ProjectileWhiteGreen.png")));

        tankTextures = new HashMap<>();
        tankTextures.put(TankStyles.GREEN_TANK, new Texture[]{new Texture(Gdx.files.internal("tanks/GreenTankBody.png")), new Texture(Gdx.files.internal("tanks/GreenTankTurret.png"))});
        tankTextures.put(TankStyles.BLUE_TANK, new Texture[]{new Texture(Gdx.files.internal("tanks/BlueTankBody.png")), new Texture(Gdx.files.internal("tanks/BlueTankTurret.png"))});
        tankTextures.put(TankStyles.LIGHT_GREEN_TANK, new Texture[]{new Texture(Gdx.files.internal("tanks/LightGreenTankBody.png")), new Texture(Gdx.files.internal("tanks/LightGreenTankTurret.png"))});

        muzzleFlash = new Texture(Gdx.files.internal("misc-images/muzzle-flash.png"));


        powerups = new HashMap<>();
        powerups.put(PowerUp.FUEL, new Texture(Gdx.files.internal("petrol.png")));
        powerups.put(PowerUp.MONEY, new Texture(Gdx.files.internal("money.png")));

        upgrades = new HashMap<>();
        upgrades.put(Upgrade.SPEED, new Texture(Gdx.files.internal("speed.png")));
        upgrades.put(Upgrade.STAMINA, new Texture(Gdx.files.internal("stamina.png")));
        upgrades.put(Upgrade.STRENGTH, new Texture(Gdx.files.internal("strength.png")));
    }

    public static synchronized AssetManager getInstance() {
        if (instance == null) {
            instance = new AssetManager();
        }
        return instance;
    }

    public Label getTextAsWhiteNonTitle(String text) {
        return getText(Color.WHITE, text, false);
    }

    public Label getText(Color color, String text, boolean title) {
        Label.LabelStyle textLableStyle = new Label.LabelStyle();
        textLableStyle.font = title ? titleFont : defaultFont;
        textLableStyle.fontColor = color;
        return new Label(text, textLableStyle);
    }

    public TextField getTextField() {
        return new TextField("", skin);
    }

    public TextButton getTextButton(String buttonText) {
        return new TextButton(buttonText, skin);
    }

    public TextButton.TextButtonStyle getCustomTextButton() {
        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = defaultFont;
        buttonStyle.up = customButton.getDrawable("rounded_rectangle_button");
        buttonStyle.down = customButton.getDrawable("rounded_rectangle_button");
        buttonStyle.checked = customButton.getDrawable("rounded_rectangle_button");
        return buttonStyle;
    }

    public ImageButton upgradeButton(Upgrade upgrade){
        ImageButton image = new ImageButton(skin, upgrade.toString());
        image.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(upgrades.get(upgrade), 32, 32));
        image.setSize(32,32);
        return image;
    }

    public ImageButton getPowerUpButton(PowerUp powerUp){
        ImageButton image = new ImageButton(skin, powerUp.toString());
        image.getStyle().imageUp = new TextureRegionDrawable(new TextureRegion(powerups.get(powerUp), 32, 32));
        image.setSize(32,32);
        return image;
    }

    public FileHandle getBackgrounds(Background background) {
        return backgrounds.get(background);
    }

    public FileHandle getGroundTexture(Background type) {
        return foregrounds.get(type);
    }

    public Texture getBullets(BulletData.BulletName bullet) {
        return bullets.get(bullet);
    }

    public String getHelpText() {
        return helpText;
    }

    public Texture[] getTankTexture(TankStyles tankStyles) {
        return tankTextures.get(tankStyles);
    }

    public Texture getMuzzleFlashTexture() {
        return muzzleFlash;
    }
}
